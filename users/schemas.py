from typing import List, Optional

from pydantic import BaseModel


class UserBase(BaseModel):
    email: str
    first_name: str
    last_name: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    is_active: bool
    is_staff: bool

    class Config:
        orm_mode = True


class Token(BaseModel):
    token: str
    user_id: str
    name: str
    email: str
    perms: list


class TokenData(BaseModel):
    email: Optional[str] = None
