from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.orm import relationship

from .database import Base


class User(Base):
    __tablename__ = "usuarios_usuario"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    first_name = Column(String)
    last_name = Column(String)
    is_staff = Column(Boolean, default=False)
    is_active = Column(Boolean, default=True)

    tokens = relationship("Token", back_populates="user")
    perms = relationship("UserPermission", back_populates="user")


class Permission(Base):
    __tablename__ = "auth_permission"

    id = Column(Integer, primary_key=True, index=True)
    codename = Column(String, index=True)
    name = Column(String, index=True)

    perms = relationship("UserPermission", back_populates="permission")



class UserPermission(Base):
    __tablename__ = "usuarios_usuario_user_permissions"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("usuarios_usuario.id"))
    user = relationship("User", back_populates="perms")
    permission_id = Column(Integer, ForeignKey("auth_permission.id"))
    permission = relationship("Permission", back_populates="perms")



class Token(Base):
    __tablename__ = "authtoken_token"

    id = Column(Integer, primary_key=True, index=True)
    created = Column(DateTime)
    user_id = Column(Integer, ForeignKey("usuarios_usuario.id"))
    user = relationship("User", back_populates="tokens")
