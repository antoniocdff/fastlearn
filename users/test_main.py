from fastapi.testclient import TestClient
from users.main import app

client = TestClient(app)

def test_login():
    response = client.post("/api-token-auth/", data={"username": "antoniocarlosdff@gmail.com", "password": "1qa1qa1qa"},
                           headers={"content-type": "application/x-www-form-urlencoded"})
    assert response.status_code == 200
    response_data = response.json()
    assert "token" in response_data
    return response_data["token"]
